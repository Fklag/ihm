#Bien écrire un programme
[Accéder au document au format PDF](./pdf/Bien_ecrire_un_programme.pdf)

## A- Documentation d’un programme : DocString.
La docstring est une chaîne de caractères que l’on n’assigne pas.  
Elle est placée juste en dessous de la signature de la fonction.  
Exemple d’une docstring sur une seule ligne :  
```python linenums="1"
def ajouter(a, b):
        """ Ajoute deux nombres l'un à l'autre et retourne le résultat."""
        return a + b
```
!!! Note "Écrire des DocStrings offrent de nombreux avantages :"
	- La fonction help() affiche cette documentation dans un interpréteur de commande (ici IDLE python).
	```python linenums="1"
	help(ajouter)
	```
	> `Help on function ajouter in module __main__:`  
	> `ajouter(a, b)`  
	> `    Ajoute deux nombres l'un à l'autre et retourne le résultat.`  

	![Popup Docstring](./images/popup_ajouter.png){style="width:40% ; float:right"}

	- Les outils de développement affichent cette documentation quand le programmeur écrit le nom de la fonction (fenêtre popup par exemple).   
	- On peut générer une bonne documentation du code avec des commandes qui extraient ces docstrings.  
	- C’est un mécanisme standardisé de documentation : tout le monde sait que si c’est là, et que ça a cette forme, c’est une documentation.  
	- On peut mettre des tests dans les docstrings, qui servent alors d’exemples d’utilisation.  

Il existe plusieurs manières de formater une docstring, et il y a même un [PEP 257](http://www.python.org/dev/peps/pep-0257/) (Python Enhancement Proposals) qui ne parle que de ça.  
*Normaly a docstring must be wrote in English !... Any programmer speak English. This avoid the accented characters.*  
Des balises standards sont mises à disposition pour formater la documentation.  

```python linenums="1"
def add(a, b):
    """ Adds two numbers and returns the result.
 
        This add two real numbers and return a real result. You will want to
        use this function in any place you would usually use the ``+`` operator
        but requires a functional equivalent.
 
        :param a: The first number to add
        :param b: The second number to add
        :type a: int
        :type b: int
        :return: The result of the addition
        :rtype: int
 
        :Example:
         >>> add(1,1)
        2
        >>> add(2.1,3.4)  # all int compatible types work
        5.5
    """
    return a + b

if __name__ == '__main__':
    import doctest 
    doctest.testmod()
```
`:::python >>>doctest.testmod()` exécute les tests de la partie `:Example:` du DocString (voir exemple ci-dessus).


## B- Tests unitaires avec pytest. Utilisation des assertions
Pour valider un projet informatique, on doit effectuer un bon nombre de tests. Une pratique de plus en plus exigée consiste à faire des tests automatiques pour chaque fonction. 
On les appelle les **tests unitaires**.

!!! Info "Fichier math_functions.py "
	```python linenums="1"
	def cube(value):  
		if isinstance(value, (int, float)):  
			cube_value = value * value * value  
		elif isinstance(value, list):  
			cube_value = [i * i * i for i in value]  
		else:  
			raise Exception("Data Type to operate has not been implemented")  
		return cube_value  
	```
		
Pour tester la fonction, on peut écrire le script `test_math_functions.py` ci-dessous.
!!! Info "Fichier test_math_functions.py "
	```python linenums="1"
	import pytest  
	from math_functions import *  
	
	def test_cube():  
		assert cube(-5) == -125, "cube(-5)==125"   
		assert cube([1, 2, 3]) == [1, 8, 27], "cube([1, 2, 3])==[1, 8, 9]"  
		
	test_cube()  
	```

Si les assertions sont validées, l’exécution du script ne renvoie rien.  
Si on modifie le test :  `assert cube(-5) == 125`, alors on obtient : 
!!! Bug "Assertion Error"
	![Assertion Error](./images/assertion_error.png){style="width:40% ; float:right"}

	
	Traceback (most recent call last):  
		File "<string>", line 420, in run_nodebug  
		File "D:\programme test\test_math_functions.py", line 8, in <module>   test_cube()  
		File "D:\programme test\test_math_functions.py", line 5, in test_cube  
			assert cube(-5)==125, 'cube(-5)==125'  
	AssertionError: cube(-5)==125  
	
	
	
## C- Mise en place des tests unitaires sous Spyder.
On reprend l’exemple de la fonction cube() de math_function.py du paragraphe B-.

!!! Note "Règles à respecter"
	- On crée, dans le même dossier que math_functions.py, autant de fichiers test que de fonctions à tester (ici il n’y en a qu’une : cube()).  
	- Le nom de tous les fichiers de tests doit commencer par test_, puis le nom de la fonction à tester. Dans notre exemple : test_cube.py.  
	- Les fichiers de tests contiennent autant de fonctions que de tests à vérifier.   

!!! Info "Fichier test_cube.py "
	```python linenums="1"  
	from math_functions import cube

	def test_cube_valeur_negative():
		assert cube(-5) == -125
    
	def test_cube_float():
		assert abs(cube(0.5) - 0.125) < 1e-10
    
	def test_cube_liste():
		assert cube([0, 1, 2, 3]) == [0, 1, 8, 27]
	```

!!! Note "Procédure : quand les fichiers sont créés, faire :"
	![Configure tests](./images/config_tests.png){style="width:25% ; float:right"}  
	
	- Exécution **&#x21E8;**  Run unit tests  
	- Sélectionner le « Test Framework » : pytest,  
	- Sélectionner l’emplacement des fichiers de tests, puis OK  
	- Les tests se lancent automatiquement dans la fenêtre Unit testing.  
	![Run tests](./images/run_tests.png){style="width:80%" margin-left="10%"}   
	- Si tout se passe bien tous les tests sont au vert,   
	- Sinon corriger les erreurs et relancer le test en cliquant sur le triangle vert : Run test.  

## D- Les pré-conditions et post-conditions.
![Exception](./images/exception.png){style="width:35% ; float:right"}
``` python linenums="1"
from math import *

def squareRoot(in_put):
    if not isinstance(in_put,(int, float)):
        raise Exception("Input data Type must be a number")
    elif in_put<0:
        raise Exception("Input data must be positive")
    else:
        value = sqrt(in_put)
        assert abs(value*value - in_put) < 1e-6, 'Square-Root false'
    return value

print(squareRoot('156'))
```
Dans cet exemple, les tests sur la variable `in_put` valide les pré-conditions.    

L’instruction `assert abs(value * value - in_put) < 1e-6, 'Square-Root false'`, est une post-condition.  

Les post-conditions sont là pour valider le code de la fonction. Il est normal de faire les tests sous forme d’assertions 
car on doit pouvoir les débrayer (exécution du code sans les assertions) une fois le code validé.  

Pour les pré-conditions, si la fonction est à usage interne (fonction privée) on utilisera des assertions, 
si elle est à usage externe (fonction publique) on utilisera des exceptions (non débrayables).  










 