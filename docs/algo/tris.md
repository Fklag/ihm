# Les tris

Pour cette partie du cours on reprend un article sur *[Les algorithmes de tris](https://interstices.info/les-algorithmes-de-tri/)* du site *Interstices.info*[^1].

Il propose notamment une animation de qualité qui permet de visualiser le fonctionnement des différents tris.

[^1]: Interstices est publiée par Inria, institut national de recherche en sciences et technologies du numérique.