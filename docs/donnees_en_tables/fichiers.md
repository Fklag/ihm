# Gestion des fichiers

## Présentation
Dans le cas de traitement des données d'un fichiers CSV par exemple, mais pas seulement, on doit pouvoir créer, ouvrir,
 en lecture et en écriture un fichier.

[Documentation officielle de la gestion des fichiers en python](https://docs.python.org/fr/3/tutorial/inputoutput.html#reading-and-writing-files)


## Ouvrir un fichier

La fonction `:::python open(nom_fichier, mode, encodage)` permet d'ouvrir un fichier en lecture ou en écriture.


|Valeur   | Mode d'accès possibles|
|:------:|:------------|
|**r**   | Ouverture en lecture seulement|
|**w**   | Ouverture en écriture seulement (la fonction crée le fichier s'il n'existe pas et l’écrase s’il existe...)|
|**a**   | Ouverture en écriture seulement avec ajout du contenu à la fin du fichier (la fonction crée le fichier s'il n'existe pas)|
|**+**   | Lorsque l'on rajoute **+ derrière r, w** ou __a__ on rajoute la lecture **et** l'écriture à leur mode d'ouverture|

**Exemple :**
```python linenums="1"
#utilisation du système d’exploitation
import os
rep_courant = os.getcwd()					# récupère le chemin du répertoire courant
print("rep_courant")						# puis l'affiche
os.chdir("/var/www")						# change de répertoire courant
print(rep_courant)							# affiche le nouveau repertoire courant

# Open a file
fo = open("foo.csv", "w", encoding="utf-8")		# fo : file_object
print ("Name of the file: ", fo.name)			# affiche "Name of the file: foo.csv"
print ("Closed or not : ", fo.closed)			# affiche "Closed or not : False"
print ("Opening mode : ", fo.mode)				# affiche "Opening mode : w"
fo.close()										# fermeture du fichier
print ("Closed or not : ", fo.closed)			# affiche "Closed or not : True"
```

## Ecrire dans un fichier

La méthode `:::python fileObject.write(str)` permet d’écrire une chaine de caractère dans un fichier ouvert.

**Exemple :**
```python linenums="1"
# Open a file
fo = open("foo.csv", "w", encoding="utf-8")		# crée le fichier s'il n'existe pas et l’écrase sinon

# création du fichier CSV de la partie "Tables CSV" (chapitre précédent)
fo.write( "Prénom,Nom,Age\nAlain,Térieur,50\nPaul,Auchon,94\nJean,Raffole,25")

# Close opened file
fo.close()
```

## Lire un fichier

La méthode `:::pythonstr = fileObject.read([count])` permet de lire une chaine de caractère depuis un fichier ouvert.  
`count` définit le nombre de caractères lus.


**Exemple : dans le fichier foo.csv créé ci-dessus**
```python linenums="1"

# Open a file
fo = open("foo.csv", "r", encoding="utf-8")
string = fo.read(8)						# string = fo.read() lit le fichier en entier
print ("Read String is : ", string)		#affiche "Prénom,N"
# Close opened file
fo.close()	
```

La méthode `:::python str = fileObject.readline([max_size])` permet de lire la ligne en cours depuis un fichier ouvert.  
`max_size` définit le nombre maximum de caractères lus dans la ligne courante.

**Exemple : dans le fichier foo.csv créé ci-dessus**
```python linenums="1"
# Open a file
fo = open("foo.csv", "r", encoding="utf-8")

line = fo.readline()
print(line)					# Affiche :  Prénom,Nom,Age

lines = fo.readline(30)
print (lines)				# Affiche :  Alain,Térieur,50
												
# Close opend file
fo.close()
```

**Exemple plus complet :** 
	Si l'on veut lever une exception si le fichier n'existe pas par exemple, puis si on veut traiter toutes les 
	données du fichier ligne par ligne, on peut écrire ce programme. 
```python linenums="1"
try:
	fo = open("foo.csv", "r", encoding="utf-8")
except:
	print("Erreur à l'ouverture du fichier")
	pass                          	# permet au programme de continuer
else:
	for line in fo :        		# lecture du ficher ligne par ligne
		line = line.strip()     	# enlève les caractères de début et de fin de ligne
		print(line)					# faire le traitement des données de la ligne en cours ici
	fichier.close()	
```			
Affiche ceci :  
```csv
	Prénom,Nom,Age
	Alain,Térieur,50
	Paul,Auchon,94
	Jean,Raffole,25
```

!!! warning "Avertissement"
	Appeler `fo.write()` sans appeler `fo.close()` pourrait mener à une situation où les arguments de `fo.write()` 
	ne seraient pas complètement écrits sur le disque, même si le programme se termine avec succès.
	
	D'ailleur pour éviter d'oublier la méthode `fo.close()`, Python propose une autre écriture :
	
	``` python linenums="1"
	with open("foo.csv", "r", encoding="utf-8") as fo:
		line = fo.readline()
	print(line)						# Fin du bloc "with" : le fichier est automatiquement fermé
	```
		
	
