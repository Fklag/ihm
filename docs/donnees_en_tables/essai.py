# Créé par Paul, le 28/06/2021 en Python 3.7

import csv
pays = []

import pandas
pays = pandas.read_csv("csv/countries.csv", delimiter=";", keep_default_na=False)
villes = pandas.read_csv("csv/cities.csv", delimiter=";")

pays_et_capitales = pandas.merge(\
    pays[['iso', 'name', 'capital', 'continent']],\
    villes[['id', 'name']].rename(\
        columns={'id': 'capital', 'name': 'capital_name'}),\
    on='capital')
