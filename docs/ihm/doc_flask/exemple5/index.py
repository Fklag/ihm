from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)

# Racine
@app.route('/')
def fonction_racine():
    # On vérifie qu'une clé est passée en paramètre
    if request.args.get('provenance'):
        return render_template("index.html", page_pre = request.args['provenance'])
    else:
        # Lors du premier accès à la page
        return render_template("index.html")

# Page 1
@app.route('/page1/')
def fonction_1():
    return render_template("page1.html", page_pre = request.args['provenance'])

# Page 2
@app.route('/page2/')
def fonction_2():
    return render_template("page2.html", page_pre = request.args['provenance'])

if __name__ == "__main__":
    app.run(debug = True)