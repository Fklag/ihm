from flask import Flask, render_template, request, jsonify

app = Flask(__name__)

# Racine
@app.route('/', methods = ['GET', 'POST'])
def fonction_racine():
    if request.method == 'POST':
        # Récupération des données du formulaire
        if request.form['operation'] == 'somme':
            resultat = int(request.form['nombre']) * 2
        else:
            resultat = int(request.form['nombre']) ** 2
        # La fonction jsonify permet de renvoyer les données dans le format JSON
        return jsonify({'resultat' : resultat})
    else:
        # Appel par défaut (méthode GET)
        return render_template('index.html')
    
if __name__ == "__main__":
    app.run(debug = True)