# On ajoute l'import de la méthode request
from flask import Flask, render_template, request, jsonify
from fonctions import cesar

# On cré une instance de la classe Flask : un serveur web
app = Flask(__name__)

# On décrit la route par défaut du serveur (racine du serveur)
@app.route("/", methods = ['POST', 'GET'])
def index():
    if request.method == 'POST':
        return jsonify({"texte_crypte" : crypter()})
    else:
        return render_template("formulaire.html")

def crypter():
    texte = request.form['texte']
    cle = int(request.form['cle'])
    if request.form['mode'] == 'decrypter':
        cle = - cle
    return cesar(texte, cle)

if __name__ == "__main__":
     # Lancement du serveur
     app.run(debug = True)  # Mode debug pour commencer