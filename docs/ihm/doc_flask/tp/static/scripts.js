// On déclenche un événement si on relâche une touche du clavier
document.addEventListener("keyup", function() {
        cryptage();
    }
);

// On déclenche un événement si on clique quelque part
document.addEventListener("click", function() {  
        cryptage();
    }
);

// Fonction appelée lorsqu'un événement est détecté sur la page 
function cryptage() {

    // on définit les valeurs des paramètres à envoyer au serveur et la méthode GET ou POST
    const data = {
        method: "POST",
        // Les données sont récupérées dans la page 
        body: new URLSearchParams({
                    texte: document.getElementById('texte').value,
                    cle: document.getElementById('cle').value,
                    mode: document.querySelector('input[name = bouton]:checked').value,
                })
    };

    // Voici la méthode permettant l'échange de données avec le serveur (allé et retour)
    // data est défini juste au dessus et contient notamment "texte" "cle" et "mode"       
    fetch("/", data)
        .then(
            // Attente d'une réponse du serveur sous forme d'un object JSON (équivalent du dictionnaire en python)
            // le nom de la variable "promesse" n'a pas d'importance
            promesse => promesse.json()
        )
        .then( 
            // Traitement de la reponse du serveur
            // le nom de la variable "reponse" n'a pas d'importance
            reponse => document.getElementById('texte_crypte').textContent = reponse["texte_crypte"]
        )
        .catch(
            // On peut gérer l'erreur si tel est le cas
            error => alert("Erreur : " + error)
        );
}