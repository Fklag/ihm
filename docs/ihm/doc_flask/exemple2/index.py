# On importe la fonction render_template
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def fonction_1():
    # On appelle la fonction render_template avec comme argument la page index.html
    return render_template("index.html")

if __name__ == "__main__":
    app.run()