<!-- # NSI au Lycée Pierre-Gilles de Gennes-->

# Présentation de MkDocs

MkDocs est un module qui permet de créer des pages HTML à partir de sources écrit en Markdown.
On bénéficie donc la simplicité de la syntaxe markdown et en utilisant les extensions de markdown, les possibilités de représentations sont surprenantes - [Petits exemples ici](https://www.carnets.info/jupyter/memomd/) :   
* barre de navigation,
* cadres intégrant des onglets,
* code colorisé en précisant le langage de programmation,
* équations mathématiques,
* cadres avec bandeau de couleur (ou non) à l'image de bootstrap,
* listes améliorées,
* notes de pied de page,
* graphes,
* texte en indice ou barré,
* intégration d'une console Basthon permettant d'exécuter du code python (ou un Jupyter) sur la page web,
* etc

# Création de la page HTML

La création de la page HTML peut se faire soit en mode local :  
`$ mkdocs new .` permet de créer un nouveau projet dans le répertoire courant (`.`indique le répertoire courant).  
`$ mkdocs serve` permet de créer un serveur local qui visualise le résultat dès qu'on change le source *.md   
`$ mkdocs build` permet de fabriquer le HTML à partir du source.  

Soit la fabrication du HTML se fait automatiquement sur le git lors du `push` des fichiers sources. Si tout est bien configuré évidemment.

[Installation et configuration de MkDocs en local](https://ens-fr.gitlab.io/mkdocs/)

[Installation et configuration de MkDocs plus complète](https://mooc-forums.inria.fr/moocnsi/t/mkdocs-une-solution-ideale/1758/2)

[Possibilités et Tuto de MkDocs](http://pageperso.lif.univ-mrs.fr/~edouard.thiel/mkdocs-et/)

[Le thème Material](https://squidfunk.github.io/mkdocs-material/)